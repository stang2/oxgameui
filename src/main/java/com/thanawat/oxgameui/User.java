/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanawat.oxgameui;

import java.io.Serializable;

/**
 *
 * @author THANAWAT_TH
 */
public class User implements Serializable{   
     char name;
     int win=0;
     int lose=0;
     int draw=0;
    
    User(char name){
        this.name=name;
    }
  
   public char getName() {
        return name;
    }
   public int win() {
        return win++;
    }

   public int lose() {
        return lose++;
    }

   public int draw() {
        return draw++;
    }

   public void setName(char name) {
        this.name = name;
    }
   public int getWin() {
        return win;
    }
   public int getLose() {
        return lose;
    }
   public int getDraw() {
        return draw;
    }

    @Override
    public String toString() {
        return "User{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
    
   
   
}
