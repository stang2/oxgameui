/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanawat.oxgameui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author THANAWAT_TH
 */
public class TestReadObject {
    public static void main(String[] args) {
            File file = null;
            FileInputStream fis = null;
            ObjectInputStream ois = null;
            User x = null;
            User o = null; 

          try {
            file = new File("ox.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            x = (User) ois.readObject();
            o = (User) ois.readObject();
            System.out.println(x);
            System.out.println(o);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestReadObject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestReadObject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestReadObject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
