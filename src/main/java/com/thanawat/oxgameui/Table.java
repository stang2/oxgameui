package com.thanawat.oxgameui;

import java.io.Serializable;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author THANAWAT_TH
 */
public class Table implements Serializable{
    char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    User X;
    User O;
    User currentPlayer;
    private User winner;
    boolean isFinish=false;
    int lastrow;
    int lastcol;
    int round=0;
    public Table(User x, User o) {
        this.X = x;
        this.O = o;
        currentPlayer = x;
    }

    void showTable() {
        System.out.println(" 1 2 3 ");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    
   
    
    User getCurrentPlayer(){
        return currentPlayer;
    }
    
    public boolean setRowCol(int row , int col){
        if(isFinish()) return false;
        if(table[row][col]=='-'){
            table[row][col]=currentPlayer.getName();
            round+=1;
            lastrow=row;
            lastcol=col;
            checkWin();
            return true;
        }
        return false;
    }
    
    public char getRowCol(int row , int col){
        return table[row][col];
    }
    
    public void switchPlayer(){
        if(currentPlayer==X){
            currentPlayer=O;
        }else{
            currentPlayer=X;
        }
    }
    
    private void setStatWinLose() {
        if(winner==X){
            X.win();
            O.lose();
        }else if (winner == null) {
            X.draw();
            O.draw();
        }else if(winner == O){
            O.win();
            X.lose();
        }
    }
    
    void checkRow(){
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol]!=currentPlayer.getName()) {
                return;
            }
        }
        isFinish = true;
        winner = currentPlayer;       
        setStatWinLose();
    }

    
    
     void checkCol(){
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col]!=currentPlayer.getName()) {
                return;
            }
        }
        isFinish = true;
        winner = currentPlayer;
        setStatWinLose();
    }
    
    void checkDiagonal(){
        if((table[0][0]==currentPlayer.getName()&&
           table[1][1]==currentPlayer.getName()&&
           table[2][2]==currentPlayer.getName())||
           (table[0][2]==currentPlayer.getName()&&
           table[1][1]==currentPlayer.getName()&&
           table[2][0]==currentPlayer.getName())){
           isFinish = true;
           winner = currentPlayer;
           setStatWinLose();
        }
        
        
    }
    void checkDraw(){
        if(round==9&&isFinish==false){
            isFinish=true;
            winner = null;
            setStatWinLose();
        }
    }
    
    public void checkWin(){
        checkCol();
        checkRow();
        checkDiagonal();
        checkDraw();
    }
    
    public boolean isFinish(){      
        return isFinish;
    }

    public User getWinner() {
        return winner;
    }

}   

